#!/bin/bash

cd ~/catkin_ws

catkin_make

cd ~/catkin_ws

source ./devel/setup.bash

sleep 5

gnome-terminal -e "roslaunch video_process video_process.launch"

sleep 5

gnome-terminal -e "roslaunch rosbridge_server rosbridge_websocket.launch"
